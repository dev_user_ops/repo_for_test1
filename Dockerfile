FROM python

RUN /usr/local/bin/python -m pip install --upgrade pip \
&& apk update \
&& apk add --virtual build-deps gcc python3-dev musl-dev \
&& apk add mariadb-dev

RUN sudo apt update

RUN sudo apt upgrade

WORKDIR ./app

ADD . /app

USER root

EXPOSE 22

EXPOSE 112233

ENV PASSWORD="qwerty123"

RUN pip install -r requirements.txt

ENTRYPOINT ["python", "manage.py", "runserver","0.0.0.0:3333"]
